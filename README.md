# bitbucket-deployment

Docker image for bitbucket deployments.

Based on amazonlinux:2017.03 which corresponds to the AMI that AWS Lambda is
running on. See also:
https://docs.aws.amazon.com/lambda/latest/dg/current-supported-versions.html

Including:
- nodejs & npm
- build dependencies
- aws cli
- ansible & boto3
