FROM amazonlinux:2023 as base

# update ssh to OpenSSH 8 - required by node 14 and npm
RUN yum install -y \
      zlib-devel \
      openssl-devel \
      shadow-utils \
      gcc-c++ \
      make \
      gzip \
      tar \
    && yum clean all

# create system user and folder
RUN mkdir /var/lib/sshd && mkdir /root/.ssh
RUN chmod -R 700 /var/lib/sshd/
RUN chown -R root:sys /var/lib/sshd/
RUN useradd -r -U -d /var/lib/sshd/ -c "sshd privsep" -s /bin/false sshd

# download ssh
RUN curl -O https://cdn.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-8.0p1.tar.gz
RUN tar -xzf openssh-8.0p1.tar.gz

# compile ssh from source
RUN cd openssh-8.0p1 && ./configure  \
  --with-privsep-path=/var/lib/sshd/ \
  --sysconfdir=/etc/ssh \
  && make && make install

# create deployment image
FROM base

# add nodesource repo
RUN curl --silent --location https://rpm.nodesource.com/setup_18.x | bash

# install packages
RUN yum install -y \
      git \
      nodejs \
      aws-cli \
      python3 \
    && yum clean all

# install the newest version of pip
RUN curl -O https://bootstrap.pypa.io/get-pip.py
RUN python3 get-pip.py --user

# install pip packages
RUN ~/.local/bin/pip3 install \
      boto3 \
      ansible

# update to latest npm
RUN npm install --global \
      npm \
      typescript

RUN npm install --global \
      aws-cdk@latest
